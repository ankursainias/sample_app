class CreatePosts < ActiveRecord::Migration[6.1]
  def change
    create_table :posts do |t|
      t.string :category
      t.string :title
      t.string :body
      t.string :email
      t.string :contact_details

      t.timestamps
    end
  end
end
