json.extract! post, :id, :category, :title, :body, :email, :contact_details, :created_at, :updated_at
json.url post_url(post, format: :json)
