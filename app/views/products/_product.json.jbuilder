json.extract! product, :id, :name, :price, :old_price, :description, :created_at, :updated_at
json.url product_url(product, format: :json)
